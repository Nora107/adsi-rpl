<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent::__construct();
		$this->load->model("user/login_model");
		$this->load->model("produk/produk_model");
		$this->datetime = date('Y-m-d H:i:s');
	
    }

	public function index()
	{
		$this->load->helper('url');
		$produk = $this->produk_model->get_data_by_id("1");
		$product = $this->produk_model->getall_produk();
		// $logins = $this->login_model->login_admin("araaeins1@gmail.com");
		// $pembayaran = $this->pembayaran_model->getall_pembayaran();
		// $catatan = $this->catatan_model->getall_catatan();
		// $data['login'] = $logins;
		$data['produk'] = $produk;
		$data['product'] = $product;
		// $data['pembayaran'] = $pembayaran;
		// $data['catatan'] = $catatan;

		$this->load->view('home', $data);
	}

	public function get_data_by_id()
	{
		$id = $this->input->get('id');

		if (!isset($id) && is_nummeric($id)) {
			$sql = "
				SELECT * FROM produk WHERE id_produk = {$id}";

			$data = $this->db->query($sql)->row();

			echo json_encode($data);
		}

	}

	public function create()
	{
		// print_r($_POST);
		if (!empty($POST)) {
			header("Content-type: application/json");
			// $this->load->library('form_validation');
			// $this->form_validation->set_rules('product_name', '<b>Nama Produk</b>', 'required|min_length[6]|max_length[15]');
            // $this->form_validation->set_rules('jenis', '<b>Jenis Produk</b>', 'required|in_list[kopi,lada]');
            // $this->form_validation->set_rules('deskripsi', '<b>Deskripsi</b>', 'required|min_length[6]|max_length[15]');

            // if ($this->form_validation->run($this) == FALSE) {

            //     $response = array(
            //         'status' => 400,
            //         'msg' => validation_errors()
            //     );
            // } else {
				// $is_error = FALSE;
				// $this->db->trans_begin();

				// try{

					$product_name = $this->input->post('product_name');
					$jenis = $this->input->post('jenis');
					$deskripsi = $this->input->post('deskripsi');

					$data = array();
					$data['product_name'] = $product_name;
					$data['jenis'] = $jenis;
					$data['deskripsi'] = $deskripsi;
					$data['created'] = $this->datetime;

					$this->db->insert('produk',$data);
					if ($this->db->affected_rows() < 0) {
                        $is_error = TRUE;
                    }
				// }catch (Exception $ex) {
                //     $is_error = TRUE;
                // }
				// if (!$is_error) {

                //     if ($this->db->trans_status() === FALSE) {
                //         $this->db->trans_rollback();

                //         // $response = array(
                //         //     'status' => 400,
                //         //     'msg' => 'Failed to add data! Please try again.'
                //         // );
                //     } else {
                //         $this->db->trans_commit();

                //         // $response = array(
                //         //     'status' => 200,
                //         //     'msg' => 'Success to add data. ',
                //         // );
                //     }
                // } else {
                //     $this->db->trans_rollback();

                //     // $response = array(
                //     //     'status' => 400,
                //     //     'msg' => 'Failed to add data! Please try again.'
                //     // );
                // }
			// }
			// echo json_encode($response);
		}
	}

	

	public function delete_data()
	{
		$id = $this->input->post('id');

		$this->db->where('id_produk', $id);
		$this->db->delete('produk');
	}
}
