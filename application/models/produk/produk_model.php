<?php 
class produk_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    
    function getall_produk(){
        $sql = "SELECT * FROM produk";
        
        return $this->db->query($sql)->result();
    }

    function get_data_by_id($id){
        $sql = "SELECT * FROM produk where id_produk = $id";

        return $this->db->query($sql)->row();
    }

    public function addProduk() {
        $data = [
            '$product_name' => $this->input->post('nama_produk'),
            '$category' => $this->input->post('jenis_produk'),
            '$gambar' => $this->input->post('gambar'),
            '$video' => $this->input->post('video'),
            '$description' => $this->input->post('description'),
            
        ];
        $this->db->insert('produk', $data);
        return $this->db->update('produk', $data);
    }

    public function editProduk($id_produk) {
        $data = array(
            '$nama' => $this->input->post('nama_produk'),
            '$jenis' => $this->input->post('jenis_produk'),
            '$gambar_produk' => $this->input->post('gambar_produk'),
            '$detail_video' => $this->input->post('detail_video'),
            '$deskripsi_produk' => $this->input->post('deskripsi_produk'),
            
        );
        if ($id_produk == 0){
            return $this->db->insert('produk', $data);
        } else {
            $this->db->where('id_produk', $id_produk);
            return $this->db->update('produk', $data);
        }
    }

    public function deleteProduk($id_produk){
        $this->db->where('id_produk', $id_produk);
        return $this->db->delete('produk');
    }

}
?>