<?php 
class pembayaran_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    
    function getall_pembayaran(){
        $sql = "SELECT * FROM pembayaran";
        
        return $this->db->query($sql)->result();
    }

    function get_data_by_id($id){
        $sql = "SELECT * FROM pembayaran where id_pembayaran = $id";

        return $this->db->query($sql)->row();
    }
}
?>