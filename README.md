# ADSI-RPL

Proyek ini dibuat untuk memenuhi tugas praktikum pada mata kuliah ADSI-RPL

## Getting started

Jalankan program XAMPP.
Lalu pergi ke C:/ xampp > htdocs
Lalu masukkan perintah dibawah ini untuk menambahkan file Codeigniter

```
git clone https://gitlab.com/Nora107/adsi-rpl.git

```
Selanjutnya buka [localhost/phpmyadmin](url)
Masukkan file 

_**taukeapp_db.sql**_

dengan cara import ke dalam database di phpmyadmin

## Jalankan aplikasi
Masuk ke [localhost/ADSI-RPL](url)

